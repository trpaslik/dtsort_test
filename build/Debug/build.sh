#!/bin/sh

CMAKE=cmake
IDE_CC=/usr/bin/clang
IDE_CXX=/usr/bin/clang++

cd $(dirname "$0")

${CMAKE} -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=Debug -DCMAKE_C_COMPILER=${IDE_CC} -DCMAKE_CXX_COMPILER=${IDE_CXX} -DCMAKE_EXPORT_COMPILE_COMMANDS=ON ../..

make

