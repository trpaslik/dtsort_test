DT-SORT tests
-------------

Before tests:

    sudo cpufreq-set -g performance

Example test:

    ./build/Release/build.sh
    SIZE=100000 ./build/Release/src/benchmark --benchmark_out_format=csv --benchmark_out=build/Release/100000.csv
    ./build/Release/src/counts 10000 10

After tests:

    sudo cpufreq-set -g powersave


